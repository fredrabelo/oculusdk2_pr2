# on Ubuntu PR2 Simulation / Real robot

	bring up the robot / gazebo

	Launch ROSBRIDGE
		
roslaunch rosbridge_server rosbridge_websocket.launch port:=9191


	Launch Mjpeg_Server
		rosrun mjpeg_server mjpeg_server _port:=8181

		In case you want to see the stream directly



			http://pr1023:8181/stream?topic=/wide_stereo/left/image_color

	// change the host and port to the ones you are using
	// I purposedly changed the ports to 8181 and 9191 cause I was using the defaults (8080/9090) for something else.

# on the javascript code

remember to set up where to get the data

	var ws = new WebSocket("ws://localhost:8888/ws"); (this is where the oculus is connected running the java server, tipically your own computer)

and

	ros.connect('ws://pr1023:9191'); (this is the ubuntu machine / pr2 c1)